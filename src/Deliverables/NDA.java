package Deliverables;
import java.util.Map;

import soot.Body;
import soot.BodyTransformer;
import soot.PackManager;
import soot.Transform;
import soot.toolkits.graph.BriefUnitGraph;
import soot.toolkits.graph.UnitGraph;


public class NDA {

	/**
	 * @param args
	 * <ul>
	 * <li>args[0] - The path to your rt.jar (not allowing phantom refs) </li>
	 * <li>args[1] - The classpath of the input </li>
	 * <li>args[2] - The class in the source dir that has the main method (needed by soot for constructing call graph)</li>
	 * <li>args[3] - The class that is to be analyzed. All classes that are referenced by this class will also be analyzed</li>
	 * </ul>
	 */
	public static void main(String[] args) {
		if(args.length < 3)
		{
			System.out.println("Insufficient arguments (see the java docs for input format)");
			System.exit(-1);
		}

//		//String[] soot_args = new String[]{		  		    
//				"-cp",
//				args[0] + ":" + args[1], 
//				//"-allow-phantom-refs",
//				"-app",
//				args[2],
//				"-i",
//				"-main-class",
//				args[3],							    
//			    "-f",
//				"jimple"
//		};
		
		String[] soot_args = new String[]{
				//"-v",		// Increase verbosity
				"-cp",
				args[0] + ":" + args[1], 
				//"-allow-phantom-refs",
				"-app",
				args[2],
				"-i",
				"java.",	 
				   "-f",
				"jimple"
				
		};
		
		Transform tf = new Transform("jtp.NullDeref", new BodyTransformer() {

			@Override			
			protected void internalTransform(Body b, String phaseName, Map options) {
				
				if(!phaseName.equals("jtp.NullDeref"))
					return;
				
				System.out.println("\nAnalyzing method: "+b.getMethod().getName());
				UnitGraph cfg = new BriefUnitGraph(b);					
				NullDrefAnalysis ndAnalysis = new NullDrefAnalysis(cfg);					
				System.out.println("\nEnd of analysis of method: "+b.getMethod().getName());
				ndAnalysis.printBuggyStatements();
			}

		   });
			
	   PackManager.v().getPack("jtp").add(tf);
	   
	   soot.Main.main(soot_args);
	}
}