package Deliverables;
import soot.Local;
import soot.SootFieldRef;
import soot.Value;


/**
 * Encapsulates reference and access path
 * 
 * @author ravi
 *
 */
public class Pointer {
	
	/**
	 * Set to true if the Pointer is an acccess path
	 */
	boolean isAP; 
	Value ref;
	SymObj symObj;
	SootFieldRef field;
	
	/**
	 * sets the access path and flags that the pointer is an access-path
	 * @param o a non null symbolic object
	 * @param f field 
	 */
	public Pointer(SymObj o, SootFieldRef f)
	{
		assert(o != null);
		
		isAP = true;
		symObj = o;
		field = f;
		ref = null;
	}
	
	/**
	 * sets the reference value and flags that the pointer is a reference  
	 * @param r should be a JimpleLocal
	 */
	public Pointer(Value r)
	{
		assert(r instanceof Local);
		assert(r != null);
		
		isAP = false;
		symObj = null;
		field = null;
		ref = r;
	}
	
	/**
	 * Cheks if the Pointer is has this object. Meaningful only if the Pointer is an access path.
	 * Used in handling method calls. 
	 * @param o
	 * @return
	 */
	public boolean hasSymObj(SymObj o)
	{
		if((symObj != null) && (symObj.equals(o)))
			return true;
		return false;
	}
		
	/**
	 * Checks if two pointers are semantically equivalent
	 */
	public boolean equals(Object o)
	{
		Pointer p = (Pointer)o;
		
		if(isAP == p.isAP)
		{
			if(isAP && symObj.equals(p.symObj) && field.name().equals(p.field.name()))
				return true;
			else if(!isAP && ref.equivTo(p.ref))
				return true;
		}
		return false;
	}	
	
	/**
	 * Hashcode overriden so that this could be used in a hashset
	 */
	public int hashCode()
	{
		return 2;
	}
	
	/**
	 * Creates an exact copy of the Pointer. i.e. r.equals(r.clone()) will always be true
	 */
	public Pointer clone()
	{
		if(isAP)
			return new Pointer(symObj,field);
		else
			return new Pointer(ref);
	}
	
	/**
	 * Creates a human readable string representation of the Pointer. 
	 */
	public String toString()
	{
		if(isAP)
			return new String("o"+symObj.getId()+"."+field.name());
		else
			return ref.toString();
	}
}
