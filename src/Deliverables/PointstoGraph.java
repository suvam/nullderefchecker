package Deliverables;
import java.util.Collection;
import java.util.HashSet;

public class PointstoGraph {
	
	/**
	 * collection of pointer->{symobjs}  mappings
	 */
	Collection<PointerMap> maps = new HashSet<PointerMap>();		
	
	/**
	 * Indicates that the points-to graph is valid/invalid
	 * Invalid points-to graph is equivalent to bot
	 */
	boolean valid = false;
	
	public void setValid()
	{
		valid = true;
	}
	
	public boolean isValid()
	{
		return valid;
	}
	
	/**
	 * Either updates the existing pointsto map or creates a new one
	 * @param ptr
	 * @param ptset
	 */
	public void setPointstoSet(Pointer ptr, Collection<SymObj> ptset)
	{
		for(PointerMap map : maps)
		{
			if(map.getPointer().equals(ptr))
			{
				map.setMapsTo(ptset);
				return;
			}				
		}
		
		//not found
		PointerMap pm = new PointerMap(ptr,ptset);
		maps.add(pm);
	}
	
	public void addMap(PointerMap pMap) {
		
		for(PointerMap map : maps)
		{
			if(map.getPointer().equals(pMap.getPointer()))
			{
				map.getMapsToSet().addAll(pMap.getMapsToSet());
				return;
			}				
		}
		
		//not found
		maps.add(pMap);		
	}
	
	public Collection<SymObj> getPointstoSet(Pointer p)
	{
		for(PointerMap map : maps)
		{
			if(map.getPointer().equals(p))
				return map.getMapsToSet();
		}
		return null;
	}
	
	public PointstoGraph clone()
	{
		PointstoGraph newPG = new PointstoGraph();
				
		for(PointerMap pm : maps)
			newPG.maps.add(pm.clone());
		
		/**
		 * also clone the valid flag
		 */
		newPG.valid = valid;		
		return newPG;
	}

	public Collection<PointerMap> getPointerMaps() //Changed to public from default
	{
		return maps;
	}
	
	public void merge(PointstoGraph pg) {
		
		//merge maps
		Collection<PointerMap> tempmaps = new HashSet<PointerMap>();		
		for(PointerMap elem : pg.getPointerMaps())
		{
			PointerMap pgmap = elem.clone();
			
			boolean found = false;
			for(PointerMap pm : getPointerMaps())
			{
				if(pm.getPointer().equals(pgmap.getPointer()))
				{
					pm.getMapsToSet().addAll(pgmap.getMapsToSet());
					found = true;
					break;
				}
			}			
			if(!found)
				tempmaps.add(pgmap);
		}
		for(PointerMap pm : tempmaps)
			getPointerMaps().add(pm);		
	}

	public void disp() 
	{	
		for(PointerMap pm : maps)
		{			
			System.out.print(pm.getPointer().toString() + "->" + "{ ");
			for(SymObj o : pm.getMapsToSet())
			{
			// null should be displayed as null	
				if(o.getId()== -1)
					System.out.print("Null" + " ,");
				else
				System.out.print("o"+o.getId() + " ,");
			}
			System.out.print(" } ,");
		}				
	}
	
	public boolean equals(Object obj)
	{
		PointstoGraph pg = (PointstoGraph)obj;
		
		//check equality of valid flags
		if(pg.valid != valid)
			return false;
		return maps.equals(pg.maps);
	}	
	
	public int hashCode()
	{
		return 4;
	}
	
	public void clear()
	{
		maps.clear();
	}
}
