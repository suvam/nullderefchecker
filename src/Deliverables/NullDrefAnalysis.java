/* Program Analysis and Verification Programming Assignment 2011

 * Code completed by Suvam Mukherjee, 		SR: 08865
 * 					 Remish Leonard Minz 	SR: 08754	
 * 
 * !!!!:MODIFIED FILE: This file contains corrections to code.
 * The commented function display(String, PointstoGraph) was used for debugging purposes.
 */

/*
 * Suvam: Fixed several major null dereference bugs
 */

package Deliverables;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.jimple.*;
import soot.jimple.internal.*;
import soot.jimple.AnyNewExpr;
import soot.jimple.AssignStmt;
import soot.jimple.Constant;
import soot.jimple.IdentityStmt;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.NullConstant;
import soot.jimple.ParameterRef;
import soot.jimple.Stmt;
import soot.jimple.ThisRef;
import soot.jimple.internal.JInstanceFieldRef;
import soot.jimple.internal.JimpleLocal;
import soot.toolkits.graph.UnitGraph;
import soot.toolkits.scalar.ForwardBranchedFlowAnalysis;


public class NullDrefAnalysis extends ForwardBranchedFlowAnalysis<PointstoGraph> {
    
	private int symObjId;	// used for unique Identification for symbolic objects
	private int buggyStmtId;
	
	//stmtToId Hash Table would map statements to unique IDs: needed for unique symbolic object generation on the fly
    private Hashtable<Stmt, Integer> stmtToId = new Hashtable<Stmt, Integer>();
    // buggyStatements is a Hash Table which stores all the statements which we suspect to have a null dereference
    private Hashtable<Stmt, Integer> buggyStatements = new Hashtable<Stmt, Integer>();
    
    private Stmt statement;	// Needed to store the current statement under processing
    
    // The following data structures are needed to process the invoke statements: they are necessary to prevent 
    // infinite loops while we traverse access paths of parameters.
    private int visitedCount;
    private SymObj visitedArray[];
    
    /**
     * constructor that accepts a UnitGraph (CFG) and calls the doAnalysis() method
     * @param graph - control flow graph
     */
    public NullDrefAnalysis(UnitGraph graph) {              
    	super(graph);
    	symObjId = 0;
    	buggyStmtId = 0;
    	visitedCount = 0;
    	visitedArray = new SymObj[500];
    	doAnalysis();
    }
        
    /**
     * The implementation of transfer function. This should invoke the transfer function corresponding to the input statement 
     * 
     * @param in (input) input points-to graph (Join of all the predecessor's points-to graphs) 
     * @param s (input) The current statement s which can be AssignStmt, IdentityStmt, IfStmt, InvokeStmt etc.
     * @param fallOut (output) a single element List that corresponds to the points-to graph of the fall out branch.
     * @param branchOuts (output) a multi element List of points-to graphs  
     * 
     */
    protected void flowThrough(PointstoGraph in, Unit s, List<PointstoGraph> fallOut, List<PointstoGraph> branchOuts) {

	/* This method contains skeleton code. You need to fill it in. 
	 * Implementation Completed!!!
	 * */

	assert(s instanceof Stmt);
	Stmt stm = (Stmt) s;
	statement = stm;
	
	/*
	System.out.println("\n--------------------------------------------------");
	System.out.println("Now processing statement: ");
	System.out.println(stm);
	System.out.println("\n--------------------------------------------------");
	*/
         
	if(stm instanceof JReturnStmt || stm instanceof JReturnVoidStmt){
		// Assume that transfer function for return statement is identity
		// display("return",in);
		
		updateOutMaps(in, fallOut, branchOuts);
	}
	else if(stm instanceof AssignStmt){
		/*
		 * In this section of the code, we implement the transfer functions for the Assignment statements
		 * As required by the question, we divide assignments into the following categories:
		 * (i)   x = r.f
		 * (ii)  r.f = x
		 * (iii) null assignment
		 * (iv)  new object assignment
		 */
			//display("Assignment",in);
			
            AssignStmt asStm = (AssignStmt) stm;
            Value left = asStm.getLeftOp();		// Get the LHS of expression
            Value right = asStm.getRightOp();	// Get the RHS of expression
            
            
            if((left instanceof JimpleLocal||left instanceof ArrayRef) && right instanceof NullConstant){
            	/*
            	 * This section of the code takes care of the following kinds of assignments:
            	 * (i)  x = null;
            	 * (ii) x[i] = null;
            	 * Transfer function: Input set S, which may or may not have x
            	 * 					  Output set: S - points-to-set(x) U {x, null}
            	 * Implementation Completed!!!
            	 */
            	
            	// display("left JimpleLocal or Array Ref, right Null", in);
            	
            	Pointer lhs = new Pointer(left);	 
            	
            	PointstoGraph inClone = in.clone();
            	Collection<SymObj> lval = in.getPointstoSet(lhs);
            	System.out.println("lval is now: "+lval);
            	
            	if(lval == null)	// "left" was not present in the IN set
            	{
            		lval = new HashSet<SymObj>();
            		lval.add(SymObj.Null());	// Update the points-to set for x to NULL
            		//System.out.println("lval is now: "+lval);
            		inClone.setPointstoSet(lhs,lval);
            		
            	}
            	else	// "left" was already present in the IN set
            	{
            		lval.clear();	// Erase whatever x was pointing to
            		lval.add(SymObj.Null());
            		inClone.setPointstoSet(lhs, lval);
            			
            	}
            
            updateOutMaps(inClone, fallOut, branchOuts);
        
            }
            
            else if( (left instanceof JimpleLocal||left instanceof ArrayRef) &&
					(right instanceof JimpleLocal||right instanceof ArrayRef) &&
					(in.getPointstoSet(getPointerForRef(right))!=null)
			){
            	/*
            	 * This section of the code takes care of the following situations:
            	 *  a = b;
            	 *  where a might be $r0 or r0[i0]
            	 *  and b might be $r0 or r0[i0]
            	 *  Implementation Completed!!!
            	 */
				Collection<SymObj> rval = new HashSet<SymObj>();
				Pointer rhs = getPointerForRef(right);
				Pointer lhs = getPointerForRef(left);
				
				/*
				 * Take the IN-set and merge it with the OUT-sets
				 */
				updateOutMaps(in, fallOut, branchOuts);
				
			
				rval = in.getPointstoSet(rhs);
				
				if (rval == null)
				{
					// Points-to set of the right did not exist in the incoming set
					// We will create a new symbolic object on the fly, and assign it here
					rval = new HashSet<SymObj> ();
				}
				
				if (rval.isEmpty())
				{
					/*
					 * Assign a new Symbolic object
					 */
					
					SymObj obj = new SymObj ();
					if (stmtToId.containsKey(stm) == false)
					{
						stmtToId.put(stm, symObjId++);
					}
							
					obj.setId(stmtToId.get(stm));
					rval.add(obj);
				}
				
				/*
				 * If we have a JimpleLocal on the left, then we are allowed to perform a strong update
				 * */
				
				if (left instanceof JimpleLocal)
				{	
					Collection<SymObj> lval = fallOut.get(0).getPointstoSet(lhs); 
					/*Strong update*/
					if (lval != null)
						lval.clear();
				}
				
				/*
				 * We have ArrayRef on the left, do weak update
				 */
				else
				{
					Collection<SymObj> lval = in.getPointstoSet(lhs); // We could have extracted this from fallOut.get(0) as well, as we have already merged
					
					/*
					 * Perform a weak update for ArrayRef on the left
					 */
					
					if (lval != null)
						rval.addAll(lval);
				}

				fallOut.get(0).setValid();
				fallOut.get(0).setPointstoSet(lhs, rval);

			}
            else if((left instanceof StaticFieldRef)||(left instanceof JimpleLocal)&&
            		(right instanceof StaticFieldRef))
            {
            	/*
            	 * To handle Java statements like System.out.println();
            	 * Idea: Do a Identity transfer function
            	 */
            	//display("left SFR/JL, right SFR",in);
            	updateOutMaps(in,fallOut,branchOuts);
            }
          
            else if(left instanceof FieldRef && right instanceof NullConstant){
            	/*
            	 * Takes care of r.f = null
            	 * Implementation Completed!!!
            	 */
            	// We are allowed to do strong updates for r.f, if the points-to set for
            	// r.f is a singleton...
            	
            		//display("left FR, right NULL", in);
            		
            		//System.out.println("\nEntering x.f=null. in is now: ");
            		
            		int allowStrongUpdate = 0; 
            	
            		AbstractInstanceFieldRef lhs = (AbstractInstanceFieldRef) left;
            		//Conversion to AbstractInstanceFieldRef necessary in order to use the function getBase()
            		
            		PointstoGraph inClone = in.clone();
            		Collection<SymObj> rPointsto = inClone.getPointstoSet(getPointerForRef(lhs.getBase()));
            		
            		if(rPointsto == null)	// r didn't appear in the Points-to graph
            		{
            			rPointsto = new HashSet<SymObj>();
      
            		}
            	
            		/*
            		 * We will now check if rPointsto null
            		 */
            		
            		
            		if(rPointsto.contains(SymObj.Null()))
            		{
            		/* Possible null dereference */
            			addToBuggyStatements(stm);
            		}
            		
            		if(rPointsto.size()==1)	// Is r's points-to graph singleton?
            		{
            			allowStrongUpdate  = 1;
            		}
            	
            		for(SymObj temp: rPointsto)
            		{
            		/* We now operate on the Points-to set of r */
            		
            			Pointer ref = new Pointer(temp, lhs.getFieldRef());
            			Collection<SymObj> refPointsto = inClone.getPointstoSet(ref);
            		
            			if(refPointsto == null)
            			{
            				// The access path didn't have a points-to map in the IN set
            				refPointsto = new HashSet<SymObj>();
            			}
            		
            			// Check whether we are allowed to do a strong update
            			if(allowStrongUpdate==1)
            			{
            				refPointsto.clear();	// Strong update
            			}
     
            			refPointsto.add(SymObj.Null());
            			
            			inClone.setPointstoSet(ref, refPointsto);
            			inClone.setValid();
            		}
            		
            		
            		updateOutMaps(inClone,fallOut,branchOuts);
            	
            	//System.out.println("\nExiting x.f= null. Fallout is: \n");
            	//fallOut.get(0).disp();
            	
            }
            
            else if( (left instanceof FieldRef) &&
		     (right instanceof JimpleLocal || right instanceof ArrayRef )&&
		     (in.getPointstoSet(getPointerForRef(right))!=null)
		     ){
            	/*
            	 * Takes care of the situation
            	 * r.f = x
            	 * r.f = x[i]
            	 */
            	//display("left FR, right JL/AR",in);
            	
            	InstanceFieldRef lhs = (InstanceFieldRef) left;
            	
            	/*
            	 * Set up a temporary object for on-the fly assignment 
            	 * for points-to set which are empty
            	 */
            	SymObj tempObj = new SymObj();
            	
            	if(stmtToId.containsKey(stm)== false)
            	{
            		stmtToId.put(stm, symObjId++);
            	}
            	tempObj.setId(stmtToId.get(stm));
       	
            	int allowStrongUpdate = 0;
            	PointstoGraph inClone = in.clone();
            	
            	Collection<SymObj> rPointsTo = inClone.getPointstoSet(getPointerForRef(lhs.getBase()));
            	Collection<SymObj> rval = inClone.getPointstoSet(getPointerForRef(right));
            	
            	//System.out.println(rPointsTo);
            	// TODO: Check what causes the nullity of rPointsTo
            	// Suvam: rPointsTo can be potentially null, temporary fix is to add a nullity check
            	if(rPointsTo!=null && rPointsTo.contains(SymObj.Null()))
            	{
            		/*
            		 * Possible null dereference
            		 */
            		addToBuggyStatements(stm);
            	}
            	
            	if(rPointsTo!= null && rPointsTo.size()==1)
            	{
            		// Strong update possible
            		
            		allowStrongUpdate = 1;
            	}
            	
            	if(rPointsTo != null) {
	            	for(SymObj temp: rPointsTo)
	            	{
	            		Pointer refPointer = new Pointer(temp, lhs.getFieldRef());
	            		Collection<SymObj> refPointsTo = inClone.getPointstoSet(refPointer);
	            		
	            		if(refPointsTo == null)
	            		{
	            			/*
	            			 * Empty points-to set, initialize new map
	            			 * and add the symbolic object created on the fly.
	            			 */
	            			refPointsTo = new HashSet<SymObj>();
	            			refPointsTo.add(tempObj);
	            		}
	            		
	            		if(allowStrongUpdate == 1)
	            		{
	            			/*
	            			 * Do strong update
	            			 */
	            			refPointsTo.clear();
	            		}
	            		refPointsTo.addAll(rval);
	            		inClone.setPointstoSet(refPointer, refPointsTo);
	            		inClone.setValid();
	            		
	            		
	            	}
            	}
            	updateOutMaps(inClone,fallOut,branchOuts);
            	
            	
	    }
            else if( (left instanceof JimpleLocal) &&
					(right instanceof FieldRef ) && !(right instanceof StaticFieldRef)
			){
            	/*
            	 * Takes care of the situation:
            	 * x = r.f
            	 */
				InstanceFieldRef f = (InstanceFieldRef) right;
				
				Pointer lhs = new Pointer(left);

				/*Merge the incoming and outgoing points-to graph.*/
				fallOut.get(0).merge(in);

				Collection<SymObj> rval = in.getPointstoSet(getPointerForRef(f.getBase()));
				Collection<SymObj> updated = new HashSet<SymObj>();
				
				/*
				 * If points-to set of r has a null, then we have a probable null-dereference...
				 */
				if (rval != null && rval.contains(SymObj.Null()))
					addToBuggyStatements(stm);

				if (rval != null)
				{
					for (SymObj obj: rval)
					{
						if (obj.equals(SymObj.Null()))
							continue;

						/*Get the points-to set of the non-null symbolic objects*/
						Pointer ptr = new Pointer (obj, f.getFieldRef());

						Collection<SymObj> ptrPointsTo = in.getPointstoSet(ptr);

						/*If o.f has no points-to elements, then add a new one.*/
						if (ptrPointsTo == null)
						{
							ptrPointsTo = new HashSet<SymObj> ();
						}

						if (ptrPointsTo.isEmpty())
						{
							SymObj o = new SymObj ();
							if (stmtToId.containsKey(stm) == false)
							{
								stmtToId.put(stm, symObjId++);
							}

							o.setId(stmtToId.get(stm));

							ptrPointsTo.add (o);
						}

						/*Now add all the points-to elements of o.f to x*/
						updated.addAll(ptrPointsTo);

					}
				}


				/*
				 * We will now do a Strong update for the points-to set of the left
				 */
				Collection<SymObj> lval = in.getPointstoSet(lhs);
				
				if (lval == null)
					lval = new HashSet<SymObj> ();
				
				lval.clear();		// Strong update
				lval.addAll(updated);
				
				
				/*
				 * Update the outgoing points-to sets
				 */
				fallOut.get(0).setPointstoSet(lhs, lval);
				fallOut.get(0).setValid();
				
				for(PointstoGraph temp: branchOuts)
				{
					temp.setPointstoSet(lhs,lval);
					temp.setValid();
				}
				
			}
            else if((left instanceof JimpleLocal) && (right instanceof JLengthExpr))
            {
            	//display("left if JL, right is JLEXPR", in);
            	/*
            	 * Takes care of the situation where we have lengthof on the right
            	 * Idea: Do a Identity transfer function
            	 */
            	updateOutMaps(in,fallOut,branchOuts);
            }
            else if( (left instanceof JimpleLocal) &&
		     (right instanceof AnyNewExpr )
		     ){
            	/*
            	 * Create a new symbolic object and assign it to points-to map
            	 */
            	//display("left JL, right ANE", in);
            	
            	SymObj temp = new SymObj();
            	PointstoGraph inClone = in.clone();
            	
            	if(stmtToId.containsKey(stm)==false)
            	{
            		stmtToId.put(stm,symObjId++);
            	}
            	temp.setId(stmtToId.get(stm));
            	
            	Pointer lhs = getPointerForRef(left);
            	Collection<SymObj> lval = inClone.getPointstoSet(lhs);
            	
            	if(lval == null)
            	{
            		/* 
            		 * New pointer
            		 */
            		lval = new HashSet<SymObj>();
            	}
            	else
            	{
            		/* 
            		 * Do a strong update...
            		 */
            		lval.clear();
            	}
            	lval.add(temp);
            	inClone.setPointstoSet(lhs, lval);
            	updateOutMaps(inClone, fallOut, branchOuts);
            	
            	//System.out.println("\nExiting x = new\n");
            	//System.out.println("\nfallOut is now: ");
            	//fallOut.get(0).disp();
            	
	    }
            else if( (left instanceof InstanceFieldRef) &&
		     (right instanceof AnyNewExpr )
		     ){
            	/*
            	 * Create a new symbolic object, but do a strong update selectively
            	 */
            	//display("left IFR, right ANE", in);
            	int allowStrongUpdate = 0;
            	
            	SymObj temp = new SymObj();
            	
            	if(stmtToId.containsKey(stm)==false)
            	{
            		stmtToId.put(stm, symObjId++);
            	}
            	temp.setId(stmtToId.get(stm));
            	
            	InstanceFieldRef f = (InstanceFieldRef) left;
            	PointstoGraph inClone = in.clone();
            	
            	Collection<SymObj> lval = inClone.getPointstoSet(getPointerForRef(f.getBase()));
            	
            	/* 
            	 * Check if the Points-to set lval contains a null.
            	 * If it does, we may have a null-dereference.
            	 * In that case, add it to the set of buggy statements.
            	 */
            	
            	if(lval.contains(SymObj.Null()))
            	{
            		addToBuggyStatements(stm);
            	}
            	
            	if(lval.size() == 1)
            	{
            		/*
            		 * A singleton Points-to set.
            		 * A strong-update is now permissible.
            		 */
            		allowStrongUpdate = 1;
            	}
            	
            	for (SymObj temp1: lval)
            	{
            		Pointer temp1Ptr = new Pointer(temp1, f.getFieldRef());
            		
            		Collection<SymObj> pointsTotemp1Ptr = inClone.getPointstoSet(temp1Ptr);
            		
            		if(pointsTotemp1Ptr == null)
            		{
            			/*
            			 * We did not have oX.f in incoming points-to set
            			 */
            			pointsTotemp1Ptr = new HashSet<SymObj>();
            		}
            		
            		if (allowStrongUpdate == 1)
            		{
            			pointsTotemp1Ptr.clear();
            		}
            		pointsTotemp1Ptr.add(temp);
            		inClone.setPointstoSet(temp1Ptr, pointsTotemp1Ptr);
            		inClone.setValid();
            		
            	}
            	updateOutMaps(in,fallOut,branchOuts);
        	
	    }
        	else if( (left instanceof JimpleLocal) &&
					(right instanceof InvokeExpr)
			){
        		/*
        		 * This section of the code takes care of method invocations.
        		 * The idea is to remove null from the access paths starting at all the parameters,
        		 * including the object starting the function call.
        		 */
        		
				InvokeExpr invokeExpr = (InvokeExpr) right;
				int argc = invokeExpr.getArgCount();	// Get the number of parameters
				PointstoGraph inClone = in.clone();

				/*
				 * We now use ValueBoxes to extract "values" in the form of Boxes
				*/
				
				if (((Value)invokeExpr).getUseBoxes().size() > 0)
				{
					ValueBox valbox = (ValueBox) ((Value)invokeExpr).getUseBoxes().get(0);
					// if x = r.m(y,z), this statement gives us r
				
					if (!(valbox instanceof JimpleLocalBox))
					{
						// We have an invocation like Math.anything
						// We are only handling $r0.something
						fallOut.get(0).clear();
						updateOutMaps(in, fallOut, branchOuts);
						return;
					}
					
					JimpleLocalBox box = (JimpleLocalBox) valbox; 
					Value invokeValue  = box.getValue();
					
					/*Here r is the points to set of r in r.m(x,y)*/
					Collection<SymObj> r = in.getPointstoSet(getPointerForRef(invokeValue));
					
					if (r != null && r.contains(SymObj.Null()))
					{
						/*
						 * Possible null dereference
						 */
						addToBuggyStatements(stm);
					}
					
					if (r != null)
					{
						/*
						 * In this section, we take the object calling the function,
						 * and remove null from all access paths beginning with this object.
						 */
						for (SymObj o: r)
						{
							flush();
							inClone = removeNullFromInvoke(o, inClone);
						}
					}
				}
		
				
				for (int i = 0; i < argc; i++)
				{
					/*
					 * In this section of the code, we remove null from the Points-to set of all
					 * the access paths beginning at the parameters passed.
					 * This section automatically invokes a recursive procedure- removeNullFromInvoke would
					 * erase null from access paths of access paths...
					 */
					Value arg = invokeExpr.getArg(i);				
					Collection<SymObj> argSet = in.getPointstoSet(getPointerForRef(arg));


					if (argSet == null)
					{
						/*
						 * skip this iteration
						 */
						continue;
					}
					

					for (SymObj o: argSet)
					{
						flush();////
						inClone = removeNullFromInvoke(o, inClone);
					}
				}
				
				updateOutMaps(inClone, fallOut, branchOuts);
				
				/*
				 * Add a new symbolic object to the points-to set of x, on the fly
				 */
				Pointer lhs = new Pointer (left);
				SymObj temp = new SymObj ();
			
				if (stmtToId.containsKey(stm) == false)
				{
					stmtToId.put(stm, symObjId++);
				}
						
				temp.setId(stmtToId.get(stm));
				
				Collection<SymObj> lval = in.getPointstoSet(lhs);
				if (lval == null)
				{
					lval = new HashSet<SymObj> ();
				}
			
				lval.clear();
				lval.add(temp);
				
				fallOut.get(0).setPointstoSet(lhs, lval);
				fallOut.get(0).setValid();
				
				for(PointstoGraph tempPG: branchOuts)
				{
					tempPG.setPointstoSet(lhs, lval);
					tempPG.setValid();
				}

			}
			else
			{
				updateOutMaps(in,fallOut,branchOuts);
			}
			
		}
	

		else if(stm instanceof InvokeStmt){
			/*
			 * Handles invokes in a similar fashion.
			 */
				InvokeStmt invoke = (InvokeStmt) stm;
	
				InvokeExpr invokeExpr = invoke.getInvokeExpr();
				int argc = invokeExpr.getArgCount();
				PointstoGraph inClone = in.clone();

				
				if (((Value)invokeExpr).getUseBoxes().size() > 0)
				{
					ValueBox valbox = (ValueBox) ((Value)invokeExpr).getUseBoxes().get(0);
					// This gives us the object which calls the method
		
					if (!(valbox instanceof JimpleLocalBox))
					{
						/*
						 * We are handling $r0.something only
						 */
							fallOut.get(0).clear();
							fallOut.get(0).merge(in);
							return;
					}
		
					JimpleLocalBox box = (JimpleLocalBox) valbox; 
					Value invokeValue  = box.getValue();
		
	
					Collection<SymObj> r = in.getPointstoSet(getPointerForRef(invokeValue));
					
					// Operation similar to the previous case...
		
					if (r != null && r.contains(SymObj.Null()))
					{
						addToBuggyStatements(stm);
					}

					if (r!= null)
					{
						for (SymObj o: r)
						{
							flush();
							inClone = removeNullFromInvoke(o, inClone);
						}
					}
				}
	
				for (int i = 0; i < argc; i++)
				{
					Value arg = invokeExpr.getArg(i);				
					Collection<SymObj> argset = in.getPointstoSet(getPointerForRef(arg));

	

					if (argset == null)
					{
						continue;
					}	
					argset.remove(SymObj.Null());

					for (SymObj o: argset)
					{
						flush();
						inClone = removeNullFromInvoke(o, inClone);
					}
				}

				fallOut.get(0).merge(inClone);
				fallOut.get(0).setValid();

		}//end of invoke stmt if
	else if(stm instanceof IdentityStmt){
		
		/*
		 * This part of the code handles statements like
		 * r0 := @this: Foo;
		 * i0 := @parameter0: int;
		 */
		//display("Identity", in);
		
	    IdentityStmt idStm = (IdentityStmt) stm;
	    Value left = idStm.getLeftOp();
	    Value right = idStm.getRightOp();
	    PointstoGraph inClone = in.clone();
                        
	    /*Saw a parameter reference on the RHS
	     * We are handling the case: i0 := @parameter0: int;
	     * */
	    if( (left instanceof JimpleLocal) && 
		(right instanceof ParameterRef || right instanceof ThisRef)
		){
	    	Pointer lhs = new Pointer(left);
	    	SymObj obj = new SymObj();
	    	
	    	/*
	    	 * The idea is to add a new symbolic object to the points-to map of the left hand side.
	    	 * The remaining components of the right are then propagated to the output branch.
	    	 */
	    	
	    	if(stmtToId.containsKey(stm)==false)
	    	{
	    		stmtToId.put(stm,symObjId++);
	    	}
	    	obj.setId(stmtToId.get(stm));
	    	
	    	Collection<SymObj> lval = new HashSet<SymObj>();
	    	lval.add(obj);
	    	
	    	inClone.setPointstoSet(lhs, lval);
	    	updateOutMaps(inClone,fallOut,branchOuts);
	    }
	}
                
	else if(stm instanceof JIfStmt){
		
	    //display("JIfStmt", in);
	    
		JIfStmt ifStm = (JIfStmt) stm;
	    Value condVal = ifStm.getCondition();
	    
	    if (condVal instanceof  JInstanceOfExpr) {
	    	/*
	    	 * Non-deterministic branch condition.
	    	 * We pass the input set to both fallOut and branchOuts.
	    	 */
	    	//display("Entering JInstanceOfExpr",in);
	    
	    	PointstoGraph inClone = in.clone();
	    	updateOutMaps(inClone,fallOut,branchOuts);
	    	
	    }
	    else if(condVal instanceof EqExpr){
	    
	    //display("Entering  JEq", in);
		EqExpr eq = (EqExpr) condVal;
		Value left = eq.getOp1();
		Value right = eq.getOp2();
		if((left instanceof NullConstant)&&
		   (right instanceof JimpleLocal || right instanceof ArrayRef)
		   ){
			
				/*
				 * Case 1: Right is a a JimpleLocal
				 */
			if(right instanceof JimpleLocal)
			{
			Pointer rhs = new Pointer(right);
			PointstoGraph inClone = in.clone();
			PointstoGraph inClone2 = in.clone();	// We would need a second copy
			    
			Collection<SymObj> rhsPointsTo = inClone.getPointstoSet(rhs);
			
			// TRUE Branch: branchOuts will contain a null in this path
			if(rhsPointsTo != null)
				rhsPointsTo.add(SymObj.Null());
			inClone.setPointstoSet(rhs, rhsPointsTo);
			
			for(PointstoGraph pg: branchOuts)
			{
				/* Update the maps for the True branch */
				pg.merge(inClone);
				pg.setValid();
			}
			
			// FALSE branch: fallOut must not contain a null in this path
			Collection<SymObj> rhsPointsTo2 = inClone2.getPointstoSet(rhs);
			PointerMap rhsMap = new PointerMap(rhs, rhsPointsTo2);
			
			rhsMap.removeNUll();
			inClone2.setPointstoSet(rhs, rhsMap.getMapsToSet());
			fallOut.get(0).merge(inClone2);
			fallOut.get(0).setValid();
			}
			/*
			 * Case 2: Right is an  ArrayRef
			 */
			if(right instanceof ArrayRef)
			{
				Pointer rhs = (getPointerForRef(right));
				Collection<SymObj> rhsPointsTo = new HashSet<SymObj>();
				PointstoGraph inClone = in.clone();
				PointstoGraph inClone2 = in.clone();
				
				// TRUE branch: add null to true branch
				rhsPointsTo = inClone.getPointstoSet(rhs);
				
				if(rhsPointsTo==null)
				{
					rhsPointsTo = new HashSet<SymObj>();
				}
				rhsPointsTo.add(SymObj.Null());
				inClone.setPointstoSet(rhs,rhsPointsTo);
				
				for(PointstoGraph temp: branchOuts)
				{
					temp.merge(inClone);
					temp.setValid();
				}
				
				// FALSE branch: remove null from false branch
				
				rhsPointsTo = new HashSet<SymObj>();
				rhsPointsTo = inClone2.getPointstoSet(rhs);
				
				
				if(rhsPointsTo == null)
				{
					rhsPointsTo = new HashSet<SymObj>();
				}
				PointerMap map = new PointerMap(rhs,rhsPointsTo);
				inClone2.addMap(map);
				fallOut.get(0).merge(inClone2);
				fallOut.get(0).setValid();
				
			}
		}
		else if((right instanceof NullConstant)&&
			(left instanceof JimpleLocal || left instanceof ArrayRef)
			){
			
			/*
			 * This section handles if(x == null)
			 * or if(x[i])
			 */
			// case 1: Left JimpleLocal
			
			if(left instanceof JimpleLocal)
			{
				Pointer lhs = new Pointer(left);
				PointstoGraph inClone = in.clone();
				PointstoGraph inClone2 = in.clone();
				
				Collection<SymObj> lhsPointsTo = inClone.getPointstoSet(lhs);
				
				// TRUE Branch: branchOuts will contain a null in this path
				if(lhsPointsTo != null)
					lhsPointsTo.add(SymObj.Null());
				inClone.setPointstoSet(lhs, lhsPointsTo);
				
				for(PointstoGraph pg: branchOuts)
				{
					/* Update the maps for the True branch */
					pg.merge(inClone);
					pg.setValid();
				}
				
				// FALSE branch: fallOut must not contain a null in this path
				Collection<SymObj> lhsPointsTo2 = inClone2.getPointstoSet(lhs);
				PointerMap lhsMap = new PointerMap(lhs, lhsPointsTo2);
				
				lhsMap.removeNUll();
				inClone2.setPointstoSet(lhs, lhsMap.getMapsToSet());
				fallOut.get(0).merge(inClone2);
				fallOut.get(0).setValid();
				}
			if(left instanceof ArrayRef)
			{
				Pointer lhs = (getPointerForRef(left));
				Collection<SymObj> lhsPointsTo = new HashSet<SymObj>();
				PointstoGraph inClone = in.clone();
				PointstoGraph inClone2 = in.clone();
				
				// TRUE branch: add null to true branch
				lhsPointsTo = inClone.getPointstoSet(lhs);
				
				if(lhsPointsTo==null)
				{
					lhsPointsTo = new HashSet<SymObj>();
				}
				lhsPointsTo.add(SymObj.Null());
				inClone.setPointstoSet(lhs,lhsPointsTo);
				
				for(PointstoGraph temp: branchOuts)
				{
					temp.merge(inClone);
					temp.setValid();
				}
				
				// FALSE branch: remove null from false branch
				
				lhsPointsTo = new HashSet<SymObj>();
				lhsPointsTo = inClone2.getPointstoSet(lhs);
				
				
				if(lhsPointsTo == null)
				{
					lhsPointsTo = new HashSet<SymObj>();
				}
				PointerMap map = new PointerMap(lhs,lhsPointsTo);
				inClone2.addMap(map);
				fallOut.get(0).merge(inClone2);
				fallOut.get(0).setValid();
			}
			
			}
				
		
		else if((left instanceof NullConstant)&&
			(right instanceof FieldRef)
			){
			/*
			 * Handling the case 
			 * if(null == r.f)
			 * Question: If Jimple is a three address representation, will this section be 
			 * ever executed? 
			 */
			
			InstanceFieldRef r =(InstanceFieldRef) right;
			Pointer rhs = new Pointer(r.getBase());
			PointstoGraph inClone = in.clone();
			PointstoGraph inClone2 = in.clone();
			
			/*
			 * TrueBranch:
			 * We will add null to every access path beginning with r
			 */
			
			for(SymObj temp: inClone.getPointstoSet(rhs))
			{
				if(!(temp.equals(SymObj.Null())))
				{
					Collection<SymObj> obj = new HashSet<SymObj>();
					obj.add(SymObj.Null());
					inClone.setPointstoSet(new Pointer(temp, r.getFieldRef()), obj);
						
				}
				
			}
			
			for(PointstoGraph temp: branchOuts)
			{
				temp.merge(inClone);
				temp.setValid();
			}
			/*
			 * False Branch: Do not remove Null from PointstoSet of every access path beginning with r 
			 */
			
			for(SymObj temp: inClone2.getPointstoSet(rhs))
			{
				if(!(temp.equals(SymObj.Null())))
				{
					Pointer ptr = new Pointer(temp, r.getFieldRef());
					Collection<SymObj> ptrPointsTo = inClone2.getPointstoSet(ptr);
					if(ptrPointsTo == null)
					{
						ptrPointsTo = new HashSet<SymObj>();
						
					}
					if(ptrPointsTo.isEmpty())
					{
						SymObj obj = new SymObj();
						if(stmtToId.containsKey(stm) == false)
						{
							stmtToId.put(stm , symObjId++);
						}
						obj.setId(stmtToId.get(stm));
						ptrPointsTo.add(obj);
					}
					PointerMap map = new PointerMap(ptr , ptrPointsTo);
					inClone2.addMap(map);
					
					
				}
			}
			
			fallOut.get(0).merge(inClone2);
			fallOut.get(0).setValid();
			
		}
		else if((right instanceof NullConstant)&&
			(left instanceof FieldRef)
			){
			/*
			 * Mirror image of the previous case.
			 * Will this case be executed?
			 * if(r.f == null)
			 */
			InstanceFieldRef f = (InstanceFieldRef)left;
			Pointer lhs = new Pointer(f.getBase());
			PointstoGraph inClone = in.clone();
			PointstoGraph inClone2 = in.clone();
			
			// TRUE branch: Add Null object to all reference paths beginning with r
			
			for(SymObj temp: inClone.getPointstoSet(lhs))
			{
				if(!(temp.equals(SymObj.Null())))
				{
					Collection<SymObj> obj = new HashSet<SymObj>();
					obj.add(SymObj.Null());
					inClone.setPointstoSet(new Pointer(temp,f.getFieldRef()),obj);
					
				}
			}
			for(PointstoGraph temp: branchOuts)
			{
				temp.merge(inClone);
				temp.setValid();
			}
			
			// FALSE branch: Do not remove null from the reference paths beginning with r
			
			for(SymObj temp: inClone2.getPointstoSet(lhs))
			{
				if(!(temp.equals(SymObj.Null())))
				{
					Pointer ptr = new Pointer(temp, f.getFieldRef());
					Collection<SymObj> ptrPointsTo = inClone2.getPointstoSet(ptr);
					if(ptrPointsTo == null)
					{
						ptrPointsTo = new HashSet<SymObj>();
						
					}
					if(ptrPointsTo.isEmpty())
					{
						SymObj obj = new SymObj();
						if(stmtToId.containsKey(stm) == false)
						{
							stmtToId.put(stm , symObjId++);
						}
						obj.setId(stmtToId.get(stm));
						ptrPointsTo.add(obj);
					}
					PointerMap map = new PointerMap(ptr , ptrPointsTo);
					inClone2.addMap(map);
					
					
				}
			}
			
			fallOut.get(0).merge(inClone2);
			fallOut.get(0).setValid();
			
		}
		else if(left instanceof JimpleLocal && right instanceof JimpleLocal){
			/*
			 * Handles the situation:
			 * if(x == y)
			 * where x and y are both JimpleLocal's.
			 * The idea is to pass the intersection of the points-to set of x and y along the 
			 * TRUE branch,
			 * and along the FALSE branch, we pass the Set difference.
			 * In fact, we wish to over-approximate the FALSE branch by passing the input set 
			 * to the output set.
			 * (A - B) U (B- A) is a subset of A U B, so the approximation is safe.
			 */
			
			PointstoGraph inClone = in.clone();
			PointstoGraph inClone2 = in.clone();
			Pointer lhs = new Pointer(left);
			Pointer rhs = new Pointer(right);
			
			Collection<SymObj> lval = new HashSet<SymObj>();
			Collection<SymObj> rval = new HashSet<SymObj>();
			Collection<SymObj> intersection = new HashSet<SymObj>();
			Collection<SymObj> xMinusy = new HashSet<SymObj>();
			Collection<SymObj> yMinusx = new HashSet<SymObj>();
			
			lval = in.getPointstoSet(lhs);
			rval = in.getPointstoSet(rhs);
				
			// TRUE branch: Take the intersection of lval and rval
			if((lval != null) && (rval != null))
			{
				for(SymObj lObj: lval)
				{
					for(SymObj rObj: rval)
					{
						if(lObj.equals(rObj))
						{
							intersection.add(rObj);
						}
					}
				}
				
				inClone.setPointstoSet(lhs, intersection);
				inClone.setPointstoSet(rhs, intersection);
				
				for(PointstoGraph temp: branchOuts)
				{
					temp.merge(inClone);
					temp.setValid();
				}
			}	
					
				/*
				 * FALSE branch:
				 * Attempt is to take a set difference
				 */
				
				// First compute the set Points-to(X) - Points-to(Y)
				
				if((lval != null) && (rval != null))
				{
					for(SymObj lObj: lval)
					{
						if(!(rval.contains(lObj)))
						{
							xMinusy.add(lObj);
						}
					}
					
					for(SymObj rObj: rval)
					{
						if(!(lval.contains(rObj)))
						{
							yMinusx.add(rObj);
						}
					}
				
				inClone2.setPointstoSet(lhs, xMinusy);
				inClone2.setPointstoSet(rhs, yMinusx);
				
				fallOut.get(0).merge(inClone2);
				fallOut.get(0).setValid();
				}
		}
		else{
			// Merge incoming and outgoing maps
			
			updateOutMaps(in, fallOut, branchOuts);
		}
                                
	    }
	    else if(condVal instanceof NeExpr){
		NeExpr ne = (NeExpr) condVal;
		Value left = ne.getOp1();
		Value right = ne.getOp2();
		if(!(left instanceof NullConstant) && !(right instanceof NullConstant))
		{
			/*
			 * What on Earth??? Code got deleted!!! Rewriting everything.
			 * Thanks Eclipse for all the help!
			 */
			
			PointstoGraph inClone = in.clone();
			PointstoGraph inClone2 = in.clone();
			
			Pointer lhs = new Pointer(left);
			Pointer rhs = new Pointer(right);
			
			Collection<SymObj> lval = new HashSet<SymObj>();
			Collection<SymObj> rval = new HashSet<SymObj>();
			Collection<SymObj> intersection = new HashSet<SymObj>();
			Collection<SymObj> xMinusy = new HashSet<SymObj>();
			Collection<SymObj> yMinusx = new HashSet<SymObj>();
			
			lval = in.getPointstoSet(lhs);
			rval = in.getPointstoSet(rhs);
			
			/*
			 *  fallOut is the FALSE branch,
			 *  So we take the Intersection of the maps of x and y here
			 */
			if((lval!=null) && (rval!=null))
			{
				for(SymObj lObj: lval)
				{
					for(SymObj rObj: rval)
					{
						if(rObj.equals(lObj))
						{
							intersection.add(rObj);
						}
					}
				}
				
				if(!(left instanceof IntConstant))
				{
					// Update left map only if it is not an Integer constant
					inClone.setPointstoSet(lhs, intersection);
				}
				
				if( !(right instanceof IntConstant))
				{
					// Update right map only if it is not an Integer constant
					inClone.setPointstoSet(rhs, intersection);
				}			
			}			
			
			// TRUE branch: We perform a set difference operation here
			
			if((lval != null) && (rval != null))
			{
				for(SymObj lObj: lval)
				{
					if(!(rval.contains(lObj)))
					{
						xMinusy.add(lObj);
					}
				}
				
				for(SymObj rObj: rval)
				{
					if(!(lval.contains(rObj)))
					{
						yMinusx.add(rObj);
					}
				}
			
			inClone2.setPointstoSet(lhs, xMinusy);
			inClone2.setPointstoSet(rhs, yMinusx);
			
			for(PointstoGraph temp: branchOuts)
			{
				temp.merge(inClone2);
				temp.setValid();
			}
			}
			
		}
		
	    
		else if((left instanceof NullConstant)&&
			(right instanceof JimpleLocal || right instanceof ArrayRef)
			)
		{
			/*
			 * Case 1: Right Instance of JimpleLocal
			 */
			
			if(right instanceof JimpleLocal)
			{
				Pointer rhs = new Pointer(right);
				PointstoGraph inClone = in.clone();
				PointstoGraph inClone2 = in.clone();
				
				// FALSE branch: clear rhs' points-to map and add null to it
				Collection<SymObj> rval = new HashSet<SymObj>();
				rval.add(SymObj.Null());
				inClone.setPointstoSet(rhs,rval);
				
				fallOut.get(0).merge(inClone);
				fallOut.get(0).setValid();
				
				// TRUE branch: remove null from points-to set
				rval = new HashSet<SymObj>();
				rval = inClone2.getPointstoSet(rhs);
				
				PointerMap pmap = new PointerMap(rhs,rval);
				pmap.removeNUll();
				inClone2.addMap(pmap);
				
				for(PointstoGraph temp: branchOuts)
				{
					temp.merge(inClone2);
					temp.setValid();
				}
				
				
			}
			
			/*
			 * Case 2: right instance of ArrayRef
			 */
			
			if(right instanceof ArrayRef)
			{
				Pointer rhs = new Pointer(((ArrayRef)right).getBase());
				PointstoGraph inClone = in.clone();
				PointstoGraph inClone2 = in.clone();
				Collection<SymObj> rval = new HashSet<SymObj>();
				rval = in.getPointstoSet(rhs);
				
				// FALSE branch: Add null object in the points-to set of array's base
				rval.add(SymObj.Null());
				inClone.setPointstoSet(rhs, rval);
				
				fallOut.get(0).merge(inClone);
				fallOut.get(0).setValid();
				
				// TRUE branch: Do not remove null from the points-to set of array's base
				for (PointstoGraph temp: branchOuts)
				{
					temp.merge(inClone2);
					temp.setValid();
				}
				
				
			
				
			}
		}
		
		else if((right instanceof NullConstant)&&
			(left instanceof JimpleLocal || left instanceof ArrayRef)
			)
		{
			/* 
			 * Mirror image of the previous case.
			 */
			
			/*
			 * Case 1: Left is JimpleLocal
			 */
			
			if(left instanceof JimpleLocal)
			{
				Pointer lhs = new Pointer(left);
				PointstoGraph inClone = in.clone();
				PointstoGraph inClone2 = in.clone();
				
				// FALSE branch: clear lhs' points-to map and add null to it
				Collection<SymObj> lval = new HashSet<SymObj>();
				lval.add(SymObj.Null());
				inClone.setPointstoSet(lhs,lval);
				
				fallOut.get(0).merge(inClone);
				fallOut.get(0).setValid();
				
				// TRUE branch: remove null from points-to set
				lval = new HashSet<SymObj>();
				lval = inClone2.getPointstoSet(lhs);
				
				PointerMap pmap = new PointerMap(lhs,lval);
				pmap.removeNUll();
				inClone2.addMap(pmap);
				
				for(PointstoGraph temp: branchOuts)
				{
					temp.merge(inClone2);
					temp.setValid();
				}
				
				
			}
			
			/*
			 * Case 2: Left instance of ArrayRef
			 */
			if(left instanceof ArrayRef)
			{
				Pointer lhs = new Pointer(((ArrayRef)left).getBase());
				PointstoGraph inClone = in.clone();
				PointstoGraph inClone2 = in.clone();
				Collection<SymObj> lval = new HashSet<SymObj>();
				lval = in.getPointstoSet(lhs);
				
				// FALSE branch: Add null object in the points-to set of array's base
				lval.add(SymObj.Null());
				inClone.setPointstoSet(lhs, lval);
				
				fallOut.get(0).merge(inClone);
				fallOut.get(0).setValid();
				
				// TRUE branch: Do not remove null from the points-to set of array's base
				for (PointstoGraph temp: branchOuts)
				{
					temp.merge(inClone2);
					temp.setValid();
				}
				
				
			
				
			}
			
			
		}
		else if((left instanceof NullConstant)&&
			(right instanceof FieldRef))
			
		{
			InstanceFieldRef f = (InstanceFieldRef) right;
			Pointer rhs = new Pointer(f.getBase());
			PointstoGraph inClone = in.clone();
			PointstoGraph inClone2 = in.clone();
			Collection<SymObj> rval = new HashSet<SymObj>();
			rval = in.getPointstoSet(rhs);
			
			/*
			 * False branch: add null to every accesspath beginning with r
			 */
			for(SymObj temp: rval)
			{
				if(!(temp.equals(SymObj.Null())))
				{
					Collection<SymObj> obj = new HashSet<SymObj>();
					obj.add(SymObj.Null());
					inClone.setPointstoSet((new Pointer(temp, f.getFieldRef())),obj);
				}
			}
			fallOut.get(0).merge(inClone);
			fallOut.get(0).setValid();
			
			/*
			 * True branch: they do not remove  null object from accesspaths
			 */
			
			for(SymObj temp:rval)
			{
				if(!(temp.equals(SymObj.Null())))
				{
					Pointer ref = new Pointer(temp,f.getFieldRef());
					Collection<SymObj> refPointsto = in.getPointstoSet(ref);
					if(refPointsto == null)
					{
						refPointsto = new HashSet<SymObj>();
					}
					if(refPointsto.isEmpty())
					{
						SymObj obj = new SymObj();
						if(stmtToId.containsKey(stm) == false)
						{
							stmtToId.put(stm, symObjId++);
						}
						obj.setId(stmtToId.get(stm));
						refPointsto.add(obj);						
					}
					inClone2.setPointstoSet(ref, refPointsto);
					
				}
				
			}
			for(PointstoGraph temp: branchOuts)
			{
				temp.merge(inClone2);
				temp.setValid();
			}
			
			
		}
		else if((right instanceof NullConstant)&&
			(left instanceof FieldRef)
			)
		{
			/* 
			 * Mirror Image of the previous situation
			 */
			InstanceFieldRef f = (InstanceFieldRef) left;
			Pointer lhs = new Pointer(f.getBase());
			PointstoGraph inClone = in.clone();
			PointstoGraph inClone2 = in.clone();
			Collection<SymObj> lval = new HashSet<SymObj>();
			lval = in.getPointstoSet(lhs);
			
			/*
			 * False branch add null to all accesspaths
			 */
			for(SymObj temp: lval)
			{
				if(!(temp.equals(SymObj.Null())))
				{
					Collection<SymObj> obj = new HashSet<SymObj>();
					obj.add(SymObj.Null());
					inClone.setPointstoSet((new Pointer(temp, f.getFieldRef())),obj);
				}
			}
			fallOut.get(0).merge(inClone);
			fallOut.get(0).setValid();
			/*
			 * True branch 
			 */
			for(SymObj temp:lval)
			{
				if(!(temp.equals(SymObj.Null())))
				{
					Pointer ref = new Pointer(temp,f.getFieldRef());
					Collection<SymObj> refPointsto = in.getPointstoSet(ref);
					if(refPointsto == null)
					{
						refPointsto = new HashSet<SymObj>();
					}
					if(refPointsto.isEmpty())
					{
						SymObj obj = new SymObj();
						if(stmtToId.containsKey(stm) == false)
						{
							stmtToId.put(stm, symObjId++);
						}
						obj.setId(stmtToId.get(stm));
						refPointsto.add(obj);						
					}
					inClone2.setPointstoSet(ref, refPointsto);
					
				}
				
			}
			for(PointstoGraph temp: branchOuts)
			{
				temp.merge(inClone2);
				temp.setValid();
			}
			
			
		}
		else
		{
			/*
			 * last case for NeEXPR
			 */
			updateOutMaps(in,fallOut,branchOuts);
			
		}
	    }
	    else
	    {
	    	/*
	    	 * last case for JIf
	    	 */
	    	updateOutMaps(in,fallOut,branchOuts);
	    }
	}
	else
	{
		/*
		 * for all cases not caught by previous clauses
		 */
		if(fallOut == null)
		{
			System.err.println("\n fallout was null");
		}
		else
		{
			if(fallOut.isEmpty())
			{
				fallOut.add(new PointstoGraph());
				
			}
			fallOut.get(0).merge(in);
			fallOut.get(0).setValid();
		}
		if(branchOuts == null)
		{
			System.err.println("\n branchOuts was null");
		}
		else
		{
			if(branchOuts.isEmpty())
			{
				branchOuts.add(new PointstoGraph());
			}
			for(PointstoGraph temp:branchOuts)
			{
				temp.merge(in);
				temp.setValid();
			}
		}
	}
	
	
    }
   
    
    /*
     * Returns a Pointer object representing ref
     * If ref is an instance of JimpleLocal, just pass it to constructor for Pointer
     * Else, if it is an instance of ArrayRef, use getBase() to get the Value, then pass it to 
     * the constructor for Pointer.
     */
    private Pointer getPointerForRef(Value ref) {
	/*
         * Implementation Complete!!!
         */
    	if(ref instanceof ArrayRef)
    	{
    		return new Pointer(((ArrayRef)ref).getBase());
    	}
    	return new Pointer(ref);
    	
    	
    }

	/**
     * Returns an empty points-to graph with the valid flag set to true 
     */
    protected PointstoGraph entryInitialFlow() {
	PointstoGraph pg = new PointstoGraph();
	pg.setValid();
	return pg; //this is the initial value
    }
        
    /**
     * Return an empty points-to graph. (Valid flag is set to false by default)
     */
    protected PointstoGraph newInitialFlow() {
	return new PointstoGraph(); //is invalid initially
    }

    /**
     * overwrites dest points-to graph with source points-to graph
     * Implementation completed!!!
     */
    protected void copy(PointstoGraph source, PointstoGraph dest) {
                        dest.clear();	// Clear the destination Points to Graph
                        dest.merge(source);	// Merge it with the source
                        dest.setValid();	// Indicate that this is a valid map
    }

    /**
     * Join of two points-to graphs.
     * Implementation completed!!!
     */
    protected void merge(PointstoGraph in1, PointstoGraph in2, PointstoGraph out) {
    	PointstoGraph in1Clone = in1.clone();
    	in1Clone.merge(in2);
    	out.merge(in1Clone);
    }

        
    /**
     * prints all the statements that are found to have a Null dereference bug 
     */
    public void printBuggyStatements() {
    		 System.out.println("\n*******************************************");
             System.out.println("\nBuggy Statements: ");
             System.out.println("\n Number of Buggy Statements Identified: "+buggyStmtId+"\n");
             System.out.println(buggyStatements);
             System.out.println("\n*******************************************");
    }
    
    /*
     * Method to update the output points-to graph.
     * fOut represents fallOut, bOut represents branchOut.
     * This method is used for all transfer functions EXCEPT conditionals,
     * where fallOut and branchOut may have to be updated in separate fashion.
     */
    public void updateOutMaps(PointstoGraph in, List<PointstoGraph> fOut, List<PointstoGraph> bOut)
    {
    	// Handle the fallOut
    	if(fOut == null)
    	{
    		System.err.println("\nError: fallOut was null...");
    		
    	}
    	else
    	{
    		if(fOut.isEmpty())
    		{
    			fOut.add(new PointstoGraph());
    		}
    		merge(fOut.get(0), in, fOut.get(0));
    		fOut.get(0).setValid();
    	}
    	// Handle the branchOut in a similar fashion
    	if(bOut == null)
    	{
    		System.err.println("\nError: branchOut was null...");
    		
    	}
    	else
    	{
    		if(bOut.isEmpty())
    		{
    			bOut.add(new PointstoGraph());
    			
    		}
    		merge(bOut.get(0), in, bOut.get(0));
    		bOut.get(0).setValid();
    	}
    }
    public void addToBuggyStatements(Stmt stm)
    {
    	if(buggyStatements.containsKey(stm)== false)
    	{
    		/* We have a new buggy statement! Hurrah! */
    		buggyStmtId++;
    		buggyStatements.put(stm, buggyStmtId);
    	}
    }
    
    public void flush(){
    	/*
    	 * The visitedArray data structure is used to keep track of the access paths we have visited.
    	 * This method clears all previous history of visits.
    	 */
		for (int i=0; i < visitedCount; i++)
			 visitedArray [i] = null;
		visitedCount = 0;
			
	}
    private PointstoGraph removeNullFromInvoke(SymObj obj, PointstoGraph in) 
    {
    	/*
    	 * From each symbolic object, remove null from its access paths.
    	 * Repeat the procedure for its access paths as well.
    	 */
    			

		visitedArray [visitedCount ++] = obj;
		
		Collection<PointerMap> pmapset = in.getPointerMaps();

		for (PointerMap map: pmapset)
		{
			Pointer ptr = map.getPointer();

			if (ptr.hasSymObj(obj))
			{
				/*
				 * Comes in only if we have an access path
				 * hasSymObj() returns true only if symobj is set
				 */
				
				Collection<SymObj> odotSet = in.getPointstoSet(ptr);

				odotSet.remove(SymObj.Null());
				in.setPointstoSet(ptr, odotSet);
				
				if(in.getPointstoSet(ptr).size() == 0)
				{
					// Add new symbolic object on the fly...
					SymObj s = new SymObj();
					
					if(stmtToId.containsKey(statement)== false)
					{
						stmtToId.put(statement, symObjId++);
					}
					s.setId(stmtToId.get(statement));
					in.getPointstoSet(ptr).add(s);
				}

				for (SymObj odot: odotSet)
				{
					
					if (alreadyVisited(odot))
						continue;
			
					removeNullFromInvoke (odot, in);
			
				}
			}
		}

		return in;
	}

	private boolean alreadyVisited(SymObj o) {
		/*
		 * Checks whether a symbolic object has already been visited.
		 * Essential to break the recursion.
		 */
		for (int i=0; i < visitedCount; i++)
			if (visitedArray [i].equals(o))
			{	
				return true;
			}
		
		return false;
	}
    
    private void display(String type, PointstoGraph in)
    {
    	System.out.println("\nNow entering: "+ type);
    	System.out.println("\n*********************************");
    	System.out.println("\nIN: ");
    	in.disp();
    	System.out.println("\n**********************************");
    }
}
